def test_replacements(mock_hub, hub):
    data = {
        "foo1": "None",
        "foo2": "__PYTHON_NONE__",
        "foo3": "xyz__PYTHON_NONE__",
        "foo4": {"foo5": "__PYTHON_NONE__"},
        "foo6": ["__PYTHON_NONE__"],
    }

    mock_hub.rend.replacements.render = hub.rend.replacements.render
    ret = mock_hub.rend.replacements.render(data)
    assert isinstance(ret, dict)
    assert ret["foo1"] == "None"
    assert ret["foo2"] is None
    assert ret["foo3"] == "xyz"
    assert ret["foo4"]["foo5"] is None
    assert isinstance(ret["foo6"], list)
    assert len(ret["foo6"]) == 1
    assert ret["foo6"][0] is None
